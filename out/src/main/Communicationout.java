package main;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.*;
import java.net.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class Communicationout extends Thread {
	private BlockingQueue<KeyEvent> enter;
	private InetAddress addr;
	protected ExecutorService execRecv;

	public Communicationout(BlockingQueue<KeyEvent> enter) {
		this.enter = enter;
		this.execRecv = Executors.newCachedThreadPool();
	}

	public void run() {
		final InetAddress addr= this.addr;
		this.execRecv.submit(new Runnable() {
			@Override
			public void run() {
				try {

					System.out.println("addr = " + addr);
					Socket socket = new Socket(addr, 8080);

					try {
						System.out.println("socket = " + socket);
						PrintWriter out = new PrintWriter(
								new BufferedWriter(new OutputStreamWriter(
										socket.getOutputStream())), true);
						for (int i = 0; i < 10000; i++) {
							String str = null;

							KeyEvent e;
							e = enter.take();
							e.getKeyCode();
							switch (e.getKeyCode()) {
							case KeyEvent.VK_Z:
								str = "z";
								break;
							case KeyEvent.VK_X:
								str = "x";
								break;
							case KeyEvent.VK_LEFT:
								str = ",";
								break;
							case KeyEvent.VK_DOWN:
								str = ".";
								break;
							case KeyEvent.VK_RIGHT:
								str = "/";
								break;
							case KeyEvent.VK_Q:
								i = 10000;
								break;

							default:
								break;
							}

							System.out.println(str);
							out.println(str);

						}
						out.println("END");
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} finally {
						System.out.println("closing...");
						socket.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
					System.exit(1);
				}
			}
		});
	}

	public void connect(String ipaddr) {

		try {
			this.addr = InetAddress.getByName(ipaddr);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.run();
	}

	public static void main(String[] args) {
		final BlockingQueue<KeyEvent> enter = new ArrayBlockingQueue<>(1000);
		Communicationout thread = new Communicationout(enter);

		final JFrame frame = new OutWindow(enter, thread);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				try {
					enter.put(e);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				frame.setVisible(true);
			}
		});
	}
}